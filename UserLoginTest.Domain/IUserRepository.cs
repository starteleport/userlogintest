﻿using System;
using System.Threading.Tasks;

namespace UserLoginTest.Domain
{
    public interface IUserRepository
    {
        Task<User> GetByLoginAsync(string login);
        Task<User> GetByIdAsync(Guid userId);
    }
}
