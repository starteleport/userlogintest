﻿using System;

namespace UserLoginTest.Domain
{
    public class User
    {
        public User(Guid id, string login, UserPassword password)
        {
            Id = id;
            Login = login;
            Password = password;
        }

        public User(string login, UserPassword password)
        {
            Id = Guid.NewGuid();
            Login = login;
            Password = password;
        }

        public Guid Id { get; }
        public string Login { get; }
        public UserPassword Password { get; }

        public string FullName { get; set; }
        public bool IsDisabled { get; set; }

        public DateTime BirthDate { get; set; }
        public string Phone { get; set; }
        public string PassportNumber { get; set; }
    }
}
