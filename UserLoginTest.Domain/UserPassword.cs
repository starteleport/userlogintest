﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace UserLoginTest.Domain
{
    public class UserPassword
    {
        public const int SaltLength = 10;

        public UserPassword(string hash, string salt)
        {
            SaltBytes = Convert.FromBase64String(salt);

            Salt = salt;
            Hash = hash;
        }

        public UserPassword(string password)
        {
            SaltBytes = SaltGenerator.CreateRandomSalt(SaltLength);

            Salt = Convert.ToBase64String(SaltBytes);
            Hash = ComputeHash(password, SaltBytes);
        }

        public string Hash { get; }
        public string Salt { get; }

        private byte[] SaltBytes { get; }

        public bool IsValidPassword(string attemptedPassword)
        {
            var attemptedHash = ComputeHash(attemptedPassword, SaltBytes);
            return Hash.Equals(attemptedHash, StringComparison.OrdinalIgnoreCase);
        }

        private string ComputeHash(string password, byte[] salt)
        {
            var saltedAttempt = ApplySalt(Encoding.UTF8.GetBytes(password), salt);

            using (var hasher = SHA384.Create())
            {
                return Convert.ToBase64String(hasher.ComputeHash(saltedAttempt));
            }
        }

        private byte[] ApplySalt(byte[] password, byte[] salt) => password.Concat(salt).ToArray();
    }
}
