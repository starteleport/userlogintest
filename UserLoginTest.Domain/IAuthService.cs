﻿using System.Threading.Tasks;
using UserLoginTest.Domain.ServiceResults;

namespace UserLoginTest.Domain
{
    public interface IAuthService
    {
        Task<ServiceResult<string>> LoginAsync(string login, string password);
    }
}