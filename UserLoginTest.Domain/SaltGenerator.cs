﻿using System.Security.Cryptography;

namespace UserLoginTest.Domain
{
    public static class SaltGenerator
    {
        public static byte[] CreateRandomSalt(int length)
        {
            byte[] randBytes;

            if (length >= 1)
            {
                randBytes = new byte[length];
            }
            else
            {
                randBytes = new byte[1];
            }

            using (var rand = new RNGCryptoServiceProvider())
            {
                rand.GetBytes(randBytes);
                return randBytes;
            }
        }
    }
}