﻿namespace UserLoginTest.Domain.ServiceResults
{
    public class ServiceResult<T>
    {
        private ServiceResult(T result)
        {
            Result = result;
            IsSuccess = true;
        }

        private ServiceResult(string errorMessage)
        {
            ErrorMessage = errorMessage;
            IsSuccess = false;
        }

        public static ServiceResult<T> Success(T result) => new ServiceResult<T>(result);
        public static ServiceResult<T> Error(string errorMessage) => new ServiceResult<T>(errorMessage);

        public bool IsSuccess { get; }
        public T Result { get; }

        public string ErrorMessage { get; }
    }
}
