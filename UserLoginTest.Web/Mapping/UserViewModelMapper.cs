using UserLoginTest.Domain;
using UserLoginTest.Web.Models;

namespace UserLoginTest.Web.Mapping
{
    public class UserViewModelMapper
    {
        public static UserViewModel Map(User user)
        {
            return new UserViewModel
            {
                Id = user.Id,
                Login = user.Login,
                Phone = user.Phone,
                FullName = user.FullName,
                BirthDate = user.BirthDate,
                PassportNumber = user.PassportNumber,
                IsEnabled = !user.IsDisabled
            };
        }
    }
}