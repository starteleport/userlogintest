﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using UserLoginTest.Domain;
using UserLoginTest.Web.Models;

namespace UserLoginTest.Web.Controllers
{
    public class AuthController : Controller
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        public async Task<IActionResult> Login()
        {
            var authenticateResult = await HttpContext.AuthenticateAsync();
            if (authenticateResult.None)
            {
                return View();
            }

            return RedirectToAction("Index", "Personal");
        }

        [HttpPost]
        public async Task<IActionResult> Login(UserLoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var loginResult = await _authService.LoginAsync(model.Login, model.Password);
            if (loginResult.IsSuccess)
            {
                await AuthenticateAsync(loginResult.Result);

                return RedirectToAction("Index", "Personal");
            }

            ModelState.AddModelError("", loginResult.ErrorMessage);

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        private async Task AuthenticateAsync(string userId)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userId)
            };

            var id = new ClaimsIdentity(
                claims,
                "ApplicationCookie",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            await HttpContext.SignInAsync(new ClaimsPrincipal(id));
        }
    }
}