﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserLoginTest.Domain;
using UserLoginTest.Web.Mapping;

namespace UserLoginTest.Web.Controllers
{
    public class PersonalController : Controller
    {
        private readonly IUserRepository _users;

        public PersonalController(IUserRepository users)
        {
            _users = users;
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            var userId = Guid.Parse(HttpContext.User.FindFirstValue(ClaimsIdentity.DefaultNameClaimType));
            var user = await _users.GetByIdAsync(userId);

            return View(UserViewModelMapper.Map(user));
        }
    }
}