﻿using System.ComponentModel.DataAnnotations;

namespace UserLoginTest.Web.Models
{
    public class UserLoginModel
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
