﻿using System;

namespace UserLoginTest.DataAccess
{
    public class UserRecord
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public string FullName { get; set; }
        public bool IsDisabled { get; set; }

        public DateTime BirthDate { get; set; }
        public string Phone { get; set; }
        public string PassportNumber { get; set; }
    }
}
