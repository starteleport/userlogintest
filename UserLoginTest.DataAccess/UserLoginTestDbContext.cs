﻿using Microsoft.EntityFrameworkCore;
using UserLoginTest.Domain;

namespace UserLoginTest.DataAccess
{
    public class UserLoginTestDbContext : DbContext
    {
        public UserLoginTestDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            FillInitialData(modelBuilder);

            modelBuilder.ApplyConfiguration(new UserRecordConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        private void FillInitialData(ModelBuilder modelBuilder)
        {
            var password = new UserPassword("$str0n9 pa$$w0rd");
            var user = new User("admin", password);

            modelBuilder.Entity<UserRecord>().HasData(UserRecordMapper.Map(user));
        }

        public DbSet<UserRecord> Users { get; set; }
    }
}
