﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using UserLoginTest.Domain;

namespace UserLoginTest.DataAccess
{
    public class UserRepository : IUserRepository
    {
        private readonly UserLoginTestDbContext _dbContext;

        public UserRepository(UserLoginTestDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<User> GetByLoginAsync(string login)
        {
            return await GetUserOrDefault(u => u.Login == login);
        }

        public async Task<User> GetByIdAsync(Guid userId)
        {
            return await GetUserOrDefault(u => u.Id == userId);
        }

        private async Task<User> GetUserOrDefault(Expression<Func<UserRecord, bool>> expression)
        {
            var userRecord = await _dbContext.Users.FirstOrDefaultAsync(expression);
            return userRecord == null ? null : UserRecordMapper.Map(userRecord);
        }
    }
}
