﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace UserLoginTest.DataAccess
{
    public class UserRecordConfiguration : IEntityTypeConfiguration<UserRecord>
    {
        public void Configure(EntityTypeBuilder<UserRecord> builder)
        {
            builder.Property(p => p.Login).HasMaxLength(100);
            builder.Property(p => p.FullName).HasMaxLength(150);
            builder.Property(p => p.Phone).HasMaxLength(50);
            builder.Property(p => p.PassportNumber).HasMaxLength(50);
            builder.Property(p => p.PasswordHash).HasMaxLength(75);
            builder.Property(p => p.PasswordSalt).HasMaxLength(25);

            builder.HasIndex(r => r.Login).IsUnique();
        }
    }
}