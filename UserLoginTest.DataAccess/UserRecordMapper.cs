﻿using UserLoginTest.Domain;

namespace UserLoginTest.DataAccess
{
    public static class UserRecordMapper
    {
        public static User Map(UserRecord userRecord)
        {
            var password = new UserPassword(userRecord.PasswordHash, userRecord.PasswordSalt);

            return new User(userRecord.Id, userRecord.Login, password)
            {
                FullName = userRecord.FullName,
                BirthDate = userRecord.BirthDate,
                IsDisabled = userRecord.IsDisabled,
                PassportNumber = userRecord.PassportNumber,
                Phone = userRecord.Phone
            };
        }

        public static UserRecord Map(User user)
        {
            return new UserRecord
            {
                Id = user.Id,
                Login = user.Login,
                PasswordHash = user.Password.Hash,
                PasswordSalt = user.Password.Salt,
                FullName = user.FullName,
                BirthDate = user.BirthDate,
                IsDisabled = user.IsDisabled,
                PassportNumber = user.PassportNumber,
                Phone = user.Phone
            };
        }
    }
}
