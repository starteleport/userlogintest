﻿using System.Threading.Tasks;
using UserLoginTest.Domain;
using UserLoginTest.Domain.ServiceResults;

namespace UserLoginTest.Application
{
    public class AuthService : IAuthService
    {
        public const string UserNotFoundErrorText = "Пользователь с данным логином и паролем не найден";
        public const string UserIsDisabledErrorText = "Пользователь заблокирован";

        private readonly IUserRepository _users;

        public AuthService(IUserRepository users)
        {
            _users = users;
        }

        public async Task<ServiceResult<string>> LoginAsync(string login, string password)
        {
            var user = await _users.GetByLoginAsync(login);

            if (user == null)
            {
                return ServiceResult<string>.Error(UserNotFoundErrorText);
            }

            if (user.IsDisabled)
            {
                return ServiceResult<string>.Error(UserIsDisabledErrorText);
            }

            if (user.Password.IsValidPassword(password))
            {
                return ServiceResult<string>.Success(user.Id.ToString());
            }

            return ServiceResult<string>.Error(UserNotFoundErrorText);
        }
    }
}
