using System;
using NUnit.Framework;
using UserLoginTest.Domain;
using UserLoginTest.Web.Mapping;

namespace UserLoginTest.Web.Tests.Mapping
{
    public class UserViewModelMapperTests
    {
        [TestCase(true, false)]
        [TestCase(false, true)]
        public void Map_FromUser_ShouldSetAllFields(bool isDisabled, bool expectedIsEnabled)
        {
            var login = "login";
            var birthDate = DateTime.Today;

            var phone = Guid.NewGuid().ToString();
            var fullName = Guid.NewGuid().ToString();
            var passportNumber = Guid.NewGuid().ToString();

            var user = new User(login, new UserPassword(Guid.NewGuid().ToString()))
            {
                BirthDate = birthDate,
                IsDisabled = isDisabled,
                Phone = phone,
                FullName = fullName,
                PassportNumber = passportNumber
            };

            var userRecord = UserViewModelMapper.Map(user);

            Assert.That(userRecord.Id, Is.EqualTo(user.Id));
            Assert.That(userRecord.Login, Is.EqualTo(login));
            Assert.That(userRecord.Phone, Is.EqualTo(phone));
            Assert.That(userRecord.FullName, Is.EqualTo(fullName));
            Assert.That(userRecord.IsEnabled, Is.EqualTo(expectedIsEnabled));
            Assert.That(userRecord.PassportNumber, Is.EqualTo(passportNumber));
        }
    }
}