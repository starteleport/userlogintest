using System;
using System.Threading.Tasks;
using Moq;
using Moq.Language.Flow;
using NUnit.Framework;
using UserLoginTest.Domain;
using UserLoginTest.Domain.ServiceResults;

namespace UserLoginTest.Application.Tests
{
    public class AuthServiceTests
    {
        private Mock<IUserRepository> _repositoryMock;
        private AuthService _sut;

        [SetUp]
        public void Setup()
        {
            _repositoryMock = new Mock<IUserRepository>();

            _sut = new AuthService(_repositoryMock.Object);
        }

        [Test]
        public async Task LoginAsync_UserNotFoundByLogin_ShouldReturnNotFoundText()
        {
            _repositoryMock.Setup(r => r.GetByLoginAsync(It.IsAny<string>())).ReturnsAsync((User)null);

            var result = await _sut.LoginAsync(Guid.NewGuid().ToString(), Guid.NewGuid().ToString());

            AssertUserNotFoundError(result);
        }

        [TestCase("valid", "invalid", false, false, true, AuthService.UserNotFoundErrorText, TestName = "User with invalid password")]
        [TestCase("valid", "valid", false, true, false, null, TestName = "User with valid password password")]
        [TestCase("valid", "valid", true, false, true, AuthService.UserIsDisabledErrorText, TestName = "Disabled user with valid password")]
        [Test]
        public async Task LoginAsync_ExistingUserAndPasswordIsCorrect_ShouldReturnNotFoundText(
            string validPassword,
            string attemptedPassword,
            bool isDisabled,
            bool isSuccessExpected,
            bool isResultEmpty,
            string expectedErrorMessage)
        {
            var login = Guid.NewGuid().ToString();
            var user = new User(login, new UserPassword(validPassword))
            {
                IsDisabled = isDisabled
            };

            SetupUser(user);

            var result = await _sut.LoginAsync(login, attemptedPassword);

            Assert.That(result.IsSuccess, Is.EqualTo(isSuccessExpected));
            Assert.That(result.ErrorMessage, Is.EqualTo(expectedErrorMessage));
            Assert.That(result.Result == null, Is.EqualTo(isResultEmpty));
        }

        private void SetupUser(User user)
        {
            _repositoryMock.Setup(r => r.GetByLoginAsync(user.Login)).ReturnsAsync(user);
        }

        private static void AssertUserNotFoundError(ServiceResult<string> result)
        {
            Assert.That(result.IsSuccess, Is.False);
            Assert.That(result.ErrorMessage, Is.EqualTo(AuthService.UserNotFoundErrorText));
        }
    }
}