using System;
using System.Diagnostics.CodeAnalysis;
using NUnit.Framework;
using UserLoginTest.Domain;

namespace UserLoginTest.DataAccess.Tests
{
    public class UserRecordMapperTests
    {
        [Test]
        [SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
        public void Map_FromUserRecord_ShouldSetAllFields()
        {
            var userId = Guid.NewGuid();
            var login = "login";
            var birthDate = DateTime.Today;
            var isDisabled = true;

            var phone = Guid.NewGuid().ToString();
            var fullName = Guid.NewGuid().ToString();
            var passportNumber = Guid.NewGuid().ToString();

            var passwordHash = "aGFzaA==";
            var passwordSalt = "c2FsdA==";

            var userRecord = new UserRecord
            {
                Login = login,
                BirthDate = birthDate,
                IsDisabled = isDisabled,
                Id = userId,
                Phone = phone,
                FullName = fullName,
                PassportNumber = passportNumber,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt
            };

            var user = UserRecordMapper.Map(userRecord);

            Assert.That(user.Id, Is.EqualTo(userId));
            Assert.That(user.Login, Is.EqualTo(login));
            Assert.That(user.Phone, Is.EqualTo(phone));
            Assert.That(user.FullName, Is.EqualTo(fullName));
            Assert.That(user.IsDisabled, Is.EqualTo(isDisabled));
            Assert.That(user.PassportNumber, Is.EqualTo(passportNumber));
            Assert.That(user.Password.Salt, Is.EqualTo(passwordSalt));
            Assert.That(user.Password.Hash, Is.EqualTo(passwordHash));
        }

        [Test]
        [SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
        public void Map_FromUser_ShouldSetAllFields()
        {
            var login = "login";
            var birthDate = DateTime.Today;
            var isDisabled = true;

            var phone = Guid.NewGuid().ToString();
            var fullName = Guid.NewGuid().ToString();
            var passportNumber = Guid.NewGuid().ToString();

            var passwordHash = "aGFzaA==";
            var passwordSalt = "c2FsdA==";

            var user = new User(login, new UserPassword(passwordHash, passwordSalt))
            {
                BirthDate = birthDate,
                IsDisabled = isDisabled,
                Phone = phone,
                FullName = fullName,
                PassportNumber = passportNumber
            };

            var userRecord = UserRecordMapper.Map(user);

            Assert.That(userRecord.Id, Is.EqualTo(user.Id));
            Assert.That(userRecord.Login, Is.EqualTo(login));
            Assert.That(userRecord.Phone, Is.EqualTo(phone));
            Assert.That(userRecord.FullName, Is.EqualTo(fullName));
            Assert.That(userRecord.IsDisabled, Is.EqualTo(isDisabled));
            Assert.That(userRecord.PassportNumber, Is.EqualTo(passportNumber));
            Assert.That(userRecord.PasswordSalt, Is.EqualTo(passwordSalt));
            Assert.That(userRecord.PasswordHash, Is.EqualTo(passwordHash));
        }
    }
}