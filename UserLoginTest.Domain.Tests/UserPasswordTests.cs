﻿using System;
using NUnit.Framework;

namespace UserLoginTest.Domain.Tests
{
    public class UserPasswordTests
    {
        [Test]
        public void Construct_WithPlainPassword_ShouldContainNonEmptyHashAndSalt()
        {
            var password = Guid.NewGuid().ToString();

            var userPassword = new UserPassword(password);

            Assert.That(userPassword.Salt, Has.Length.GreaterThan(10));
            Assert.That(userPassword.Hash, Has.Length.GreaterThan(48));
        }

        [Test]
        public void IsValidPassword_WithCorrectPassword_ShouldBeTrue()
        {
            var password = Guid.NewGuid().ToString();

            var userPassword = new UserPassword(password);

            Assert.That(userPassword.IsValidPassword(password), Is.True);
        }
    }
}
