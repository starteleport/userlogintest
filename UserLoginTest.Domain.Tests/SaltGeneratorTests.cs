﻿using NUnit.Framework;

namespace UserLoginTest.Domain.Tests
{
    public class SaltGeneratorTests
    {
        [TestCase(0, 1, TestName = "When asked for zero bytes, should return one byte")]
        [TestCase(5, 5, TestName = "When asked for non-zero bytes, should return as asked")]
        public void CreateRandomSalt_Always_ShouldRespectCount(int askedBytes, int expectedBytes)
        {
            var actual = SaltGenerator.CreateRandomSalt(askedBytes);

            Assert.That(actual.Length, Is.EqualTo(expectedBytes));
        }
    }
}
